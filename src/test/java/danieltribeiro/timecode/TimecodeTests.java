package danieltribeiro.timecode;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TimecodeTests {

    @Test public void testAdd() {
        Timecode t1 = Timecode.parse("01:00:00:00", "29.97");
        Timecode t2 = Timecode.parse("00:01:02:03", "29.97");
        Timecode t3 = t1.add(t2);
        assertArrayEquals(new int[]{1, 1, 2, 3}, t3.getComponents());
    }

    @Test public void addingDifferentFrameRatesShouldThrowError() {
        Timecode t1 = Timecode.parse("01:00:00:00", "29.97");
        Timecode t2 = Timecode.parse("00:01:02:03", "30.00");
        try {
            t1.add(t2);
            fail("diffferent framerates should throw an error");
        } catch (Exception e) {
        }
    }

  @Test public void testParseFromString() {
    Timecode tc = Timecode.parse("01:00:00:00", "29.97");
    assertEquals(3600000, tc.getTotalMilis());
  }

  @Test public void testParseFromStringWithDifferentSeparator() {
    Timecode tc = Timecode.parse("01:00:00.00", "29.97");
    assertEquals("01:00:00.00", tc.toString());
    tc = Timecode.parse("01:00:00;00", "29.97");
    assertEquals("01:00:00;00", tc.toString());
  }


  @Test public void testParseFromTimestamp() {
    Timecode tc = Timecode.parse("01:00:00.033", "29.97");
    assertEquals(3600033, tc.getTotalMilis());
  }

  @Test public void testParseFromTimestampWithComma() {
    Timecode tc = Timecode.parse("01:00:00,033", "29.97");
    assertEquals(3600033, tc.getTotalMilis());
  }

  @Test public void testFromDouble() {
    Timecode tc = Timecode.parse("3600033.001", "29.97");
    assertEquals(3600033, tc.getTotalMilis());
  }

  @Test public void testConvertFromTimestamp() {
    Timecode sourceValue = Timecode.parse("01:02:03.456", "30.00");
    Timecode tc = sourceValue.convertFrameRate("29.97", "3600000");
    assertArrayEquals(new int[]{1, 2, 3, 18}, tc.getComponents());
  }

  @Test public void testConvert() {
    Timecode sourceValue = Timecode.parse("02:00:00:00", "30.00");
    Timecode tc = sourceValue.convertFrameRate("29.97", "3600000");
    assertArrayEquals(new int[]{2, 0, 3, 18}, tc.getComponents());
  }

}