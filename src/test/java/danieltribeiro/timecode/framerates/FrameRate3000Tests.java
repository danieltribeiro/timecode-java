package danieltribeiro.timecode.framerates;

import org.junit.Test;

import danieltribeiro.timecode.Timecode;

import static org.junit.Assert.*;


public class FrameRate3000Tests {

  FrameRate3000 frameRate = new FrameRate3000();

  @Test
  public void testParse() {
    Timecode tc = frameRate.parse("01:00:00:00");
    assertEquals(3600000, tc.getTotalMilis());
    assertEquals(108000, tc.getTotalFrames());
    assertEquals(1, tc.getHours());
    assertEquals(0, tc.getMinutes());
    assertEquals(0, tc.getSeconds());
    assertEquals(0, tc.getFrames());
  } 
}
