package danieltribeiro.timecode.framerates;

public class FrameRate2997 extends AbstractFrameRate {

  public FrameRate2997() {
    super("29.97", 29.97, true);
  }

  public int calculateDroppedFramesOnInterval(long frameCount) {
      int D = (int) (frameCount / 17982);
      int M = (int) (frameCount % 17982);
      return (18 * D) + 2 * ((M - 2) / 1798);
  }
}