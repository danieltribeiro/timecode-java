package danieltribeiro.timecode.framerates;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FrameRateProvider {

  private static FrameRateProvider INSTANCE;

  private Map<String, FrameRate> frameRatesMap = null;

  private FrameRateProvider() {
    Map<String, FrameRate> map = new HashMap<>();
    putInMap(new FrameRate2398(), map);
    putInMap(new FrameRate2500(), map);
    putInMap(new FrameRate2997(), map);
    putInMap(new FrameRate3000(), map);
    this.frameRatesMap = map;
  }

  public synchronized static FrameRateProvider getInstance() {
    if (INSTANCE == null) {
        INSTANCE = new FrameRateProvider();
    }
    return INSTANCE;
  }

  
  public FrameRate getFrameRate(String name) {
    return getFrameRatesMap().get(name);
  }

  public Collection<FrameRate> getFrameRates() {
    return getFrameRatesMap().values();
  }
  
  public Collection<String> getFrameRateNames() {
    return getFrameRatesMap().keySet();
  }
  
  /**
   * @return the frameRatesMap
   */
  public Map<String, FrameRate> getFrameRatesMap() {
    return frameRatesMap;
  }
  
  private void putInMap(FrameRate frameRate, Map<String, FrameRate> map) {
    map.put(frameRate.getName(), frameRate);
  }
}