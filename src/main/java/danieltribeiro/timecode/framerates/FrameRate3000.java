package danieltribeiro.timecode.framerates;

public class FrameRate3000 extends AbstractFrameRate {

  public FrameRate3000() {
    super("30.00", 30.00, false);
  }

  @Override
  public String getName() {
    return "30.00";
  }

  public int calculateDroppedFramesOnInterval(long frameCount) {
      return 0;
  }
}