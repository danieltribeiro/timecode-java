package danieltribeiro.timecode.framerates;

import danieltribeiro.timecode.Timecode;
import danieltribeiro.timecode.TimecodeParam;

public abstract class AbstractFrameRate implements FrameRate {

  private final String name;

  private final double rate;

  private final boolean dropFrame;

  public AbstractFrameRate(String name, double rate, boolean dropFrame) {
    this.name = name;
    this.rate = rate;
    this.dropFrame = dropFrame;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public boolean isDropFrame() {
    return this.dropFrame;
  }

  public double getRate() {
    return this.rate;
  }

  public int getRoundedRate() {
    return (int)Math.ceil(this.rate);
  }

  @Override
  public String toString() {
    return getRoundedRate() + (isDropFrame() ? "DF" : "NDF");
  }

  @Override
  public Timecode parseParam(TimecodeParam param) {
    switch (param.getParamType()) {
      case TIMECODE:
        return parse(param.getValue().toString());
      case FRAME_COUNT:
        return parseFromTotalFrames(Integer.parseInt(param.getValue().toString()));
      case MILLISECONDS:
        return parseFromTotalMilis(Integer.parseInt(param.getValue().toString()));
      default:
        throw new IllegalArgumentException("Cannot parse timecode param " + param);
    }
  }

  @Override
  public Timecode parseFromTotalMilis(int milis) {
    int frameCount = frameCountFromMilis(milis);
    int[] components = componentsFromFrameCount(frameCount);
    return new Timecode(
      milis, 
      frameCount, 
      components[0], 
      components[1], 
      components[2], 
      components[3], 
      this);
  }

  public Timecode parseFromTotalFrames(int frameCount) {
    int milis = milisFromFrameCount(frameCount);
    int[] components = componentsFromFrameCount(frameCount);
    return new Timecode(
      milis, 
      frameCount, 
      components[0], 
      components[1], 
      components[2], 
      components[3], 
      this);
  }
  public Timecode parse(String s) {
    String[] arr = s.split("[^0-9']+");
    String separator = s.substring(8, 9);
    Timecode tc = parseFromComponents(asInt(arr[0]), asInt(arr[1]), asInt(arr[2]), asInt(arr[3]));
    tc.setFrameSeparator(separator);
    return tc;
  }

  private int asInt(String s) {
    return Integer.parseInt(s);
  }

  public Timecode parseFromComponents(int[] components) {
    return parseFromComponents(components[0], components[1], components[2], components[3]);
  }

  public Timecode parseFromComponents(int hours, int minutes, int seconds, int frames) {
    int frameCount = frameCountFromComponents(hours, minutes, seconds, frames);
    int milis = milisFromFrameCount(frameCount);
    return new Timecode(
      milis,
      frameCount, 
      hours, 
      minutes, 
      seconds, 
      frames, 
      this);
  }

  private int frameCountFromComponents(int hours, int minutes, int seconds, int frames) {
    int roundedFrameRate = getRoundedRate();

    int frameNumber = (3600 * roundedFrameRate) * hours + (60 * roundedFrameRate) * minutes + roundedFrameRate * seconds
        + frames;

    int droppedFrames = calculateDroppedFramesOnInterval(frameNumber);
    frameNumber -= droppedFrames;
    
    return frameNumber;

  }

  private double getFrameDuration() {
    return (1000.0 / getRate());
  }

  private int frameCountFromMilis(int milis) {
    double duration = getFrameDuration();
    int result = (int)Math.ceil((double) milis / duration);
    return result;
  }

  private int milisFromFrameCount(int frameCount) {
    double frameInterval = 1000.0 / getRate();
    double milis = (double)frameCount * frameInterval;
    return (int)milis;
  }

  private int[] componentsFromFrameCount(int frameCount) {
    frameCount += calculateDroppedFramesOnInterval(frameCount);

    int roundedFrameRate = getRoundedRate();
    int frames = frameCount % roundedFrameRate;
    int seconds = (int) Math.floor(frameCount / (roundedFrameRate)) % 60;
    int minutes = (int) Math.floor((frameCount / (roundedFrameRate)) / 60) % 60;
    int hours = (int) Math.floor(((frameCount / (roundedFrameRate)) / 60) / 60) % roundedFrameRate;
    return new int[] {hours, minutes, seconds, frames};
  }

  public int calculateDroppedFramesOnInterval(Timecode startTimecode, Timecode endTimecode) {
    return calculateDroppedFramesOnInterval(endTimecode.getTotalMilis() - startTimecode.getTotalMilis());
  }

  public abstract int calculateDroppedFramesOnInterval(long frameCount);

}