package danieltribeiro.timecode.framerates;

public class FrameRate2500 extends AbstractFrameRate {

  public FrameRate2500() {
    super("25.00", 25.00, true);
  }

  public int calculateDroppedFramesOnInterval(long frameCount) {
      return 0;
  }
}