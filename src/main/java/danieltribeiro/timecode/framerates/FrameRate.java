package danieltribeiro.timecode.framerates;

import danieltribeiro.timecode.Timecode;
import danieltribeiro.timecode.TimecodeParam;

/**
 * O Frame Rate representa uma base de tempo do SMPTE.
 * Diferentes classes 
 */
public interface FrameRate {

  public Timecode parseFromTotalMilis(int milis);

  public Timecode parseFromTotalFrames(int frameCount);

  public Timecode parseFromComponents(int hours, int minutes, int seconds, int frames);

  public Timecode parse(String s);

  public Timecode parseParam(TimecodeParam param);

  /**
   * @return the name
   */
  public String getName();

  /**
   * @return the dropFrame
   */
  public boolean isDropFrame();

  /**
   * @return int the rounded Rate
   */
  public int getRoundedRate();

  public double getRate();
}