package danieltribeiro.timecode.framerates;

public class FrameRate2398 extends AbstractFrameRate {

  public FrameRate2398() {
    super("23.98", 23.98, true);
  }

  public int calculateDroppedFramesOnInterval(long frameCount) {
    return (int) Math.ceil(frameCount * 0.0008333);
  
  }

}