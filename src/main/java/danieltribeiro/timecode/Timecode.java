package danieltribeiro.timecode;

import danieltribeiro.timecode.framerates.FrameRate;
import danieltribeiro.timecode.framerates.FrameRateProvider;

public class Timecode {

  private static String defaultFrameSeparator = ":";

  private final int totalMilis;

  private final int frameCount;

  private final int hours;
  
  private final int minutes;
  
  private final int seconds;
  
  private final int frames;
  
  private final FrameRate framerate;

  private String frameSeparator;


  public Timecode(int totalMilis, int totalFrames, int hours, int minutes, int seconds, int frames, FrameRate framerate) {
    this.totalMilis = totalMilis;
    this.frameCount = totalFrames;
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    this.frames = frames;
    this.framerate = framerate;
  }


  /**
   * @return the frameCount
   */
  public int getFrameCount() {
    return frameCount;
  }

  /**
   * @param frameSeparator the frameSeparator to set
   */
  public void setFrameSeparator(String frameSeparator) {
    this.frameSeparator = frameSeparator;
  }

  /**
   * @return the frameSeparator
   */
  public String getFrameSeparator() {
    return frameSeparator;
  }

  /**
   * @return the hours
   */
  public int getHours() {
    return hours;
  }

  /**
   * @return the minutes
   */
  public int getMinutes() {
    return minutes;
  }

  /**
   * @return the seconds
   */
  public int getSeconds() {
    return seconds;
  }

  /**
   * @return the frames
   */
  public int getFrames() {
    return frames;
  }

  /**
   * @return the totalFrames
   */
  public int getTotalFrames() {
    return frameCount;
  }

  /**
   * @return the totalMilis
   */
  public int getTotalMilis() {
    return totalMilis;
  }

  /**
   * @return the components
   */
  public int[] getComponents() {
    return new int[] {hours, minutes, seconds, frames};
  }

  /**
   * @return the framerate
   */
  public FrameRate getFramerate() {
    return framerate;
  }

    /**
   * @return the framerate
   */
  public FrameRate getFrameRate() {
    return framerate;
  }

  @Override
  public String toString() {
    if (frameSeparator != null) {
      return format(frameSeparator);
    } else {
      return format(defaultFrameSeparator);
    }
  }

  /**
   * Format this timecode using the delimiter for separating the frame part
   * @param frameDelimiter
   * @return
   */
  public String format(String frameDelimiter) {
    return f2d(hours) + ":" + f2d(minutes) + ":" + f2d(seconds) + frameDelimiter + f2d(frames);
  }

  private static String f2d(int val) {
    if (val > -1 && val < 100) {
      if (val < 10) {
        return "0" + val;
      } else {
        return ""+val;
      }
    }
    throw new IllegalArgumentException("Somente valores entre 0 e 99 podem ser formatados nos timecodes");
  }

  /**
   * Get the separator to be used at the frame part on the timecode string representation
   * @return the defaultFrameSeparator
   */
  public static String getDefaultFrameSeparator() {
    return defaultFrameSeparator;
  }

  /**
   * Set the separator to be used at the frame part on the timecode string representation
   * @param defaultFrameSeparator the defaultFrameSeparator to set
   */
  public static void setDefaultFrameSeparator(String defaultFrameSeparator) {
    Timecode.defaultFrameSeparator = defaultFrameSeparator;
  }

  /**
   * 
   * @param param The param to be parsed.
   * @param frameRate The FrameRate of the timecode
   * @return
   */
  public static Timecode parse(int value, FrameRate frameRate) {
    return parse(""+value, frameRate);
  }

    /**
   * 
   * @param param The param to be parsed.
   * @param frameRate The FrameRate of the timecode
   * @return
   */
  public static Timecode parse(String value, String frameRate) {
    return parse(TimecodeParam.parse(value), FrameRateProvider.getInstance().getFrameRate(frameRate));
  }

  /**
   * 
   * @param param The param to be parsed.
   * @param frameRate The FrameRate of the timecode
   * @return
   */
  public static Timecode parse(TimecodeParam param, String frameRate) {
    return parse(param, FrameRateProvider.getInstance().getFrameRate(frameRate));
  }

  /**
   * 
   * @param param The param to be parsed.
   * @param frameRate The FrameRate of the timecode
   * @return
   */
  public static Timecode parse(String param, FrameRate frameRate) {
    return parse(TimecodeParam.parse(param), frameRate);
  }

  /**
   * 
   * @param param The param to be parsed
   * @param frameRate The FrameRate of the timecode
   * @return
   */
  public static Timecode parse(TimecodeParam param, FrameRate frameRate) {
    return frameRate.parseParam(param);
  }

  /**
   * 
   * @param newFrameRate The target FrameRate which this Timecode will be converted
   * @param offset The base offset to be used on drop frames relative to this timecode
   * @return The new Timecode converted to the new FrameRate
   */
  public Timecode convertFrameRate(String newFrameRate, String offset) {
    Timecode baseOffset = Timecode.parse(offset, getFramerate());
    FrameRate targetFrameRate = FrameRateProvider.getInstance().getFrameRate(newFrameRate);
    return new DefaultFrameRateConverter().convert(this, getFramerate(), targetFrameRate, baseOffset);
  }

    /**
   * 
   * @param targetFrameRate The target FrameRate which this Timecode will be converted
   * @param offset The base offset to be used on drop frames relative to this timecode
   * @return The new Timecode converted to the new FrameRate
   */
  public Timecode convertFrameRate(FrameRate targetFrameRate, String offset) {
    Timecode baseOffset = Timecode.parse(offset, getFramerate());
    return new DefaultFrameRateConverter().convert(this, getFramerate(), targetFrameRate, baseOffset);
  }

  /**
   * 
   * @param targetFrameRate The target FrameRate which this Timecode will be converted
   * @param offset The base offset to be used on drop frames relative to this timecode
   * @return The new Timecode converted to the new FrameRate
   */
  public Timecode convertFrameRate(FrameRate targetFrameRate, Timecode offset) {
    return new DefaultFrameRateConverter().convert(this, getFramerate(), targetFrameRate, offset);
  }

  public Timecode add(Timecode timecodeToAdd) {
      if (!(getFrameRate().equals(timecodeToAdd.getFrameRate()))) {
          throw new RuntimeException("Cannot sum timecodes in different framerates");
      }
      return parse((getFrameCount() + timecodeToAdd.getFrameCount()) + "f", getFramerate());
  }

}