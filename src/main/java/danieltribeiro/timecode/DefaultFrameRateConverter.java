package danieltribeiro.timecode;

import danieltribeiro.timecode.framerates.FrameRate;
import java.util.HashMap;
import java.util.Map;

public class DefaultFrameRateConverter implements FrameRateConverter {

  Map<String, Map<String, Double>> conversionMap;

  public DefaultFrameRateConverter() {
    this.conversionMap = new HashMap<>();
    conversionMap.put("30.00", getConversionFrom3000fps());
    conversionMap.put("29.97", getConversionFrom2997fps());
    conversionMap.put("25.00", getConversionFrom2500fps());
    conversionMap.put("23.98", getConversionFrom2398fps());
  }

  private Map<String, Double> getConversionFrom3000fps() {
    Map<String, Double> map = new HashMap<>();
    // map.put("30.00", 1.0);
    // map.put("29.97", 1.001);
    // map.put("25.00", 0.96);
    // map.put("23.98", 1.0);
    map.put("30.00", 0.0);
    map.put("29.97", 0.001);
    map.put("25.00", -0.04);
    map.put("23.98", 0.0);
    return map;
  }

  private Map<String, Double> getConversionFrom2997fps() {
    Map<String, Double> map = new HashMap<>();
    // map.put("30.00", 0.999000999000999);
    // map.put("29.97", 1.0);
    // map.put("25.00", 0.959042);
    // map.put("23.98", 0.999015277777778);
    map.put("30.00", -0.001);
    map.put("29.97", 0.0);
    map.put("25.00", -0.040958);
    map.put("23.98", -0.001);
    return map;
  }

  private Map<String, Double> getConversionFrom2500fps() {
    Map<String, Double> map = new HashMap<>();
    // map.put("30.00", 1.41666);
    // map.put("29.97", 1.042709);
    // map.put("25.00", 1.0);
    // map.put("23.98", 1.04166666666667);
    map.put("30.00", 0.041666666666667);
    map.put("29.97", 0.042694639);
    map.put("25.00", 0.0);
    map.put("23.98", 0.041666666666667);
    return map;
  }

  private Map<String, Double> getConversionFrom2398fps() {
    Map<String, Double> map = new HashMap<>();
    // map.put("30.00", 1.0);
    // map.put("29.97", 1.001);
    // map.put("25.00", 0.96);
    // map.put("23.98", 1.0);
    map.put("30.00", 0.0);
    map.put("29.97", 0.001);
    map.put("25.00", -0.04);
    map.put("23.98", 0.0);
    return map;
  }

  @Override
  public Timecode convert(Timecode sourceValue, FrameRate sourceFrameRate, FrameRate targetFrameRate,
      Timecode baseOffset) {
    return convert(sourceValue.getTotalMilis(), sourceFrameRate, targetFrameRate, baseOffset.getTotalMilis());
  }

  public Timecode convert(int sourceMilis, FrameRate sourceFrameRate, FrameRate targetFrameRate,
      int baseMilis) {

    double factor = conversionMap.get(sourceFrameRate.getName()).get(targetFrameRate.getName());
    int adjust = (int) ((sourceMilis - baseMilis) * factor);
    int newMilis = sourceMilis + adjust;
    return targetFrameRate.parseFromTotalMilis(newMilis);
  }


}