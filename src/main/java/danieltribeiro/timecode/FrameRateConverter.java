package danieltribeiro.timecode;

import danieltribeiro.timecode.framerates.FrameRate;

public interface FrameRateConverter {
  
  public Timecode convert(Timecode sourceValue, FrameRate sourceFrameRate, FrameRate targetFrameRate, Timecode baseOffset);

  public Timecode convert(int sourceMilis, FrameRate sourceFrameRate, FrameRate targetFrameRate, int baseMilis);


}