package danieltribeiro.timecode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class TimecodeParam {

  public static enum ParamType { TIMECODE, FRAME_COUNT, MILLISECONDS }

  private ParamType paramType;

  private Object value;

  public TimecodeParam() {

  }

  public static TimecodeParam parse(String s) {
    final Pattern timecodePattern = Pattern.compile("\\d\\d\\:\\d\\d\\:\\d\\d[\\:\\.\\;]\\d\\d");
    final Pattern frameCountPattern = Pattern.compile("\\d+f");
    final Pattern millisecondsPattern = Pattern.compile("\\d+(\\.\\d+)?");
    final Pattern timestampPattern = Pattern.compile("\\d\\d:\\d\\d:\\d\\d[\\.\\,]\\d\\d\\d");
    
    TimecodeParam timecodeParam = new TimecodeParam();
    if (timecodePattern.matcher(s).matches()) {
      timecodeParam.setParamType(ParamType.TIMECODE);
      timecodeParam.value = s;

    } else if (frameCountPattern.matcher(s).matches()) {
      timecodeParam.setParamType(ParamType.FRAME_COUNT);
      timecodeParam.value = Integer.parseInt(s.substring(0, s.length()-1));

    } else if (millisecondsPattern.matcher(s).matches()) {
      timecodeParam.setParamType(ParamType.MILLISECONDS);
      timecodeParam.value = (int) (Double.parseDouble(s));

    } else if (timestampPattern.matcher(s).matches()) {
      timecodeParam.setParamType(ParamType.MILLISECONDS);
      try {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        timecodeParam.value = df.parse(s.replaceAll(",", ".")).getTime();
      } catch (ParseException e) {
        throw new IllegalArgumentException("Could not parse " + s, e);
	    }
    } else {
      throw new IllegalArgumentException("Could not parse " + s);
    }
    return timecodeParam;
  }

  /**
   * @param paramType the paramType to set
   */
  public void setParamType(ParamType paramType) {
    this.paramType = paramType;
  }

  /**
   * @return the paramType
   */
  public ParamType getParamType() {
    return paramType;
  }

  /**
   * @param value the value to set
   */
  public void setValue(Object value) {
    this.value = value;
  }

  /**
   * @return the value
   */
  public Object getValue() {
    return value;
  }
}